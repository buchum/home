var express = require('express');
const router = express.Router();
const session = require('express-session');
const { check, validationResult } = require('express-validator');
const mailgun = require("mailgun-js");


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Welcome to our Home' });
});

router.get('/contact-us', function(req, res, next) {
  console.log(req.session);
  res.render('contact-us', {
        title: 'Contact Us',
        success: req.session.success || false,
        errors: req.session.errors || [],
        name: req.session.name || '',
        email: req.session.email || '',
        textMessage: req.session.textMessage || ''
  });

  req.session.errors = null;
});


router.post('/contact-us',
    [
        check('name')
            .not()
            .isEmpty()
            .withMessage('Name is required'),
        check('email', 'Email is required')
            .isEmail(),
        check('textMessage', 'Please write something for us to read.')
            .isLength({ min: 10 })
    ], (req, res) => {
      const errors = validationResult(req);
       if (!errors.isEmpty()) {
            req.session.errors = errors.array();
            req.session.success = false;
            req.session.name = req.body.name;
            req.session.email = req.body.email;
            req.session.textMessage = req.body.textMessage;

            res.redirect('/contact-us');
        } else {
            const DOMAIN = "mg.bouthavy.com";
            const mg = mailgun({apiKey: "526e5a455850cc2896f5e7946fcde23a-c322068c-a8fe1760", domain: DOMAIN});
            const data = {
	             from: "Homie Site <postmaster@mg.bouthavy.com>",
	              to: 'bouthavy@gmail.com',
	               subject: "Inquiry on home",
	                text: req.body.email + "\n" + req.body.name + "\n" + req.body.textMessage
            };

            mg.messages().send(data, function (error, body) {
            	console.log(body);
            });

            req.session.success = true;
            req.session.errors = [];
            req.session.name = '';
            req.session.email = '';
            req.session.textMessage = '';
            res.redirect('/contact-us');
        }
    });

module.exports = router;
